package net.steve.kryo;

import java.util.HashMap;

public class TestDataClass {

	private HashMap<String, String> testDataMap;
	private String testDataString;

	public TestDataClass() {
		testDataMap = new HashMap<String, String>();
		testDataString = "";
	}

	public void addNewFolderSyncKey(String id, String data) {
		testDataMap.put(id, data);
	}

	public String getFolderSyncKey(String id) {
		return testDataMap.get(id);
	}

	public String getTestDataString() {
		return testDataString;
	}

	public void setTestDataString(String testDataString) {
		this.testDataString = testDataString;
	}

	public int getMapSize() {
		return testDataMap.size();
	}

	public HashMap<String, String> getTestDataMap() {
		return testDataMap;
	}

	public void setTestDataMap(HashMap<String, String> testDataMap) {
		this.testDataMap = testDataMap;
	}

}
