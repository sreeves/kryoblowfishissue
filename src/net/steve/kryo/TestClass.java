package net.steve.kryo;

import java.util.HashMap;

public class TestClass {

	private HashMap<Integer, TestDataClass> testData;

	public TestClass() {
		testData = new HashMap<Integer, TestDataClass>();
	}

	public void addData(int id, TestDataClass data) {
		testData.put(id, data);
	}

	public TestDataClass getData(int accountId) {
		TestDataClass data = testData.get(accountId);
		if (data == null) {
			data = new TestDataClass();
		}
		return data;
	}

	public HashMap<Integer, TestDataClass> getTestData() {
		return testData;
	}

	public void setTestData(HashMap<Integer, TestDataClass> testData) {
		this.testData = testData;
	}
}
