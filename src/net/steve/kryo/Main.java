package net.steve.kryo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.crypto.KeyGenerator;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.BlowfishSerializer;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

public class Main {

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException {
		Kryo kryo = new Kryo();
		byte[] key = KeyGenerator.getInstance("Blowfish").generateKey().getEncoded();

		kryo.register(TestClass.class, new BlowfishSerializer(new FieldSerializer<TestClass>(kryo, TestClass.class), key));
		kryo.register(TestDataClass.class, new BlowfishSerializer(new FieldSerializer<TestDataClass>(kryo, TestDataClass.class), key));
		kryo.register(HashMap.class);

		TestClass testData = createTestDataClasses();

		serializeTestClass(kryo, testData);

		TestClass readData = deserializeTestClass(kryo);
		if (readData == null) {
			System.out.println("read data was null.");
		} else {
			System.out.println("read data was fine. Found: " + readData.getTestData().size());
		}

	}

	private static TestClass createTestDataClasses() {
		TestClass test = new TestClass();
		TestDataClass testData = test.getData(1);
		testData.setTestDataString("data testing string");
		testData.addNewFolderSyncKey("1", "this is the data");
		test.addData(1, testData);

		TestDataClass testData2 = test.getData(2);
		testData2.setTestDataString("data string 2");
		testData2.addNewFolderSyncKey("2", "other data");
		test.addData(2, testData2);

		return test;
	}

	private static TestClass deserializeTestClass(Kryo kryo) {
		Input input = null;
		TestClass test = null;
		try {
			File f = new File(System.getProperty("java.io.tmpdir"), "tmpFile");

			if (!f.exists()) {
				return new TestClass();
			}

			input = new Input(new FileInputStream(f));

			test = (TestClass) kryo.readClassAndObject(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				input.close();
			}
		}
		return test;
	}

	private static void serializeTestClass(Kryo kryo, TestClass testClass) {
		if (testClass == null || testClass.getTestData().size() == 0) {
			return;
		}

		FileOutputStream fos;
		Output output = null;
		try {
			File f = new File(System.getProperty("java.io.tmpdir"), "tmpFile");
			if (!f.exists()) {
				try {
					f.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			fos = new FileOutputStream(f);
			output = new Output(fos);

			kryo.writeClassAndObject(output, testClass);
			output.flush();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (output != null) {
				output.close();
			}
		}

	}

}
